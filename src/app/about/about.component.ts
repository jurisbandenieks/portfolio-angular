import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.scss"],
})
export class AboutComponent implements OnInit {
  skills = [
    { name: "HTML", img: "95" },
    { name: "CSS", img: "85" },
    { name: "SASS", img: "85" },
    { name: "Javascript", img: "80" },
    { name: ".NET", img: "60" },
    { name: "Typescript", img: "80" },
    { name: "Angular", img: "80" },
    { name: "React", img: "70" },
    { name: "Vue", img: "80" },
    { name: "Node", img: "70" },
    { name: "Git", img: "85" },
    { name: "SQL", img: "70" },
    { name: "MongoDB", img: "70" },
    { name: "Firebase", img: "80" },
  ];
  courses = [
    {
      title: "Gatsby JS & Shopify: Gatsby ecommerce sites [Gatsby 2021]",
      certificate:
        "https://www.udemy.com/certificate/UC-3b41662c-b792-4e43-8e31-3e24b07006f5/",
      url: "udemy.com/course/gatsby-ecommerce-shopify/",
      gitlab: "https://gitlab.com/jurisbandenieks/gatsby-shopify",
      imgUrl: "assets/img/shopify.jpg",
    },
    {
      title: "Modern HTML & CSS From The Beginning (Including Sass)",
      certificate: "https://www.udemy.com/certificate/UC-4DNYBRKO/",
      url: "udemy.com/course/modern-html-css-from-the-beginning/",
      gitlab: "https://gitlab.com/jurisbandenieks/vanilla",
      imgUrl: "assets/img/html.png",
    },
    {
      title: "Modern JavaScript From The Beginning",
      certificate: "",
      url: "https://www.udemy.com/course/modern-javascript-from-the-beginning/",
      gitlab: "https://gitlab.com/jurisbandenieks/vanilla",
      imgUrl: "assets/img/js.png",
    },

    {
      title: "The Complete JavaScript Course 2020: Build Real Projects!",
      certificate: "",
      url: "https://www.udemy.com/course/the-complete-javascript-course/",
      gitlab: "https://gitlab.com/jurisbandenieks/natours-sass",
      imgUrl: "assets/img/js.png",
    },
    {
      title: "React Front To Back",
      certificate: "https://www.udemy.com/certificate/UC-FY89MVH8/",
      url: "https://www.udemy.com/course/modern-react-front-to-back/",
      gitlab: "https://gitlab.com/jurisbandenieks/reactjs",
      imgUrl: "assets/img/reactjs.png",
    },
    {
      title: "Complete Vuejs Course: Vue.js + Nuxt.js + PHP + Express.js",
      certificate: "https://www.udemy.com/certificate/UC-OJ7990GU/",
      url: "https://www.udemy.com/course/vuejs-for-students-with-zero-es6-foundation/",
      gitlab: "",
      imgUrl: "assets/img/vue.png",
    },
    {
      title: "Vue JS 2 - The Complete Guide (incl. Vue Router & Vuex)",
      certificate: "https://www.udemy.com/certificate/UC-KQUFYA9L/",
      url: "https://www.udemy.com/course/vuejs-2-the-complete-guide/",
      gitlab: "",
      imgUrl: "assets/img/vue.png",
    },
    {
      title: "Build Web Apps with Vue JS 2 & Firebase",
      certificate: "https://www.udemy.com/certificate/UC-55X5ME4G/",
      url: "https://www.udemy.com/course/build-web-apps-with-vuejs-firebase/",
      gitlab: "https://gitlab.com/jurisbandenieks/vue-firebase",
      imgUrl: "assets/img/vue.png",
    },
    {
      title: "Nuxt.js - Vue.js on Steroids",
      certificate: "https://www.udemy.com/certificate/UC-W82DFMGD/",
      url: "https://www.udemy.com/course/nuxtjs-vuejs-on-steroids/",
      gitlab: "https://gitlab.com/jurisbandenieks/blog-nuxt",
      imgUrl: "assets/img/nuxt.png",
    },
    {
      title: "Angular Front To Back",
      certificate: "https://www.udemy.com/certificate/UC-FY89MVH8/",
      url: "https://www.udemy.com/course/angular-4-front-to-back/",
      gitlab: "https://gitlab.com/jurisbandenieks/angularjs",
      imgUrl: "assets/img/angularjs.png",
    },
    {
      title: "Angular - The Complete Guide (2020 Edition)",
      certificate: "",
      url: "https://www.udemy.com/course/the-complete-guide-to-angular-2/",
      gitlab: "https://gitlab.com/jurisbandenieks/angularjs",
      imgUrl: "assets/img/angularjs.png",
    },
    {
      title: "Unit Testing and Test Driven Development in NodeJS",
      certificate: "https://www.udemy.com/certificate/UC-LISSY793/",
      url: "https://www.udemy.com/course/unit-testing-and-test-driven-development-in-nodejs/",
      gitlab: "https://gitlab.com/jurisbandenieks/utesting",
      imgUrl: "assets/img/nodejs.png",
    },
    {
      title: "C# Basics for Beginners: Learn C# Fundamentals by Coding",
      certificate: "https://www.udemy.com/certificate/UC-QTVR0ZX7/",
      url: "https://www.udemy.com/course/csharp-tutorial-for-beginners/",
      gitlab: "",
      imgUrl: "assets/img/.net.png",
    },
    {
      title: "GitLab CI: Pipelines, CI/CD and DevOps for Beginners",
      certificate: "https://www.udemy.com/certificate/UC-52ZV44RP/",
      url: "https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/",
      gitlab: "https://gitlab.com/jurisbandenieks/static-website",
      imgUrl: "assets/img/gitlab.png",
    },
    {
      title: "Stripe In Practice",
      certificate:
        "https://www.udemy.com/certificate/UC-0ebbe91c-16e0-49c6-a424-714c726350ed/",
      url: "https://www.udemy.com/course/stripe-course/",
      gitlab: "https://gitlab.com/jurisbandenieks/stripe-course",
      imgUrl: "assets/img/stripe.png",
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
