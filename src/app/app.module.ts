import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { MaterialModule } from "./material.module";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { WelcomeComponent } from "./welcome/welcome.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { AboutComponent } from "./about/about.component";
import { WorkComponent } from "./work/work.component";
import { HeaderComponent } from "./navigation/header/header.component";
import { DrawerComponent } from "./navigation/drawer/drawer.component";
import { FooterComponent } from "./navigation/footer/footer.component";
import { ShowcaseComponent } from "./welcome/showcase/showcase.component";
import { ShowcaseItemComponent } from "./welcome/showcase/showcase-item/showcase-item.component";
import { SkillsComponent } from "./about/skills/skills.component";
import { CoursesComponent } from "./about/courses/courses.component";
import { ProjectComponent } from "./work/project/project.component";
import { ResumeComponent } from './resume/resume.component';
import { ProjectDescriptionComponent } from './work/project/project-description/project-description.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NotFoundComponent,
    AboutComponent,
    WorkComponent,
    HeaderComponent,
    DrawerComponent,
    FooterComponent,
    ShowcaseComponent,
    ShowcaseItemComponent,
    SkillsComponent,
    CoursesComponent,
    ProjectComponent,
    ResumeComponent,
    ProjectDescriptionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
