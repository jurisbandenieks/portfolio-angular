import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-drawer",
  templateUrl: "./drawer.component.html",
  styleUrls: ["./drawer.component.scss"]
})
export class DrawerComponent implements OnInit {
  @Output() closeSidenav = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onClose() {
    this.closeSidenav.emit();
  }
}
