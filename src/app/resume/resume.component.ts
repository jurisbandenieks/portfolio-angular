import { Component } from "@angular/core";
import { ResumeService } from "./resume.service";

@Component({
  selector: "app-resume",
  templateUrl: "./resume.component.html",
  styleUrls: ["./resume.component.scss"],
})
export class ResumeComponent {
  public experience = [];
  public education = [];

  constructor(private resumeService: ResumeService) {
    this.experience = this.resumeService.experience;

    this.education = this.resumeService.education;
  }
}
