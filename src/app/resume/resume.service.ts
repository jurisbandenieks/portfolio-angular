import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class ResumeService {
  public experience = [
    {
      company: {
        name: "GetQuorum",
        url: "https://www.getquorum.com/",
        location: "Toronto, Canada",
        description:
          "BunkerEx is an online portal for owners and operators to find their optimal bunker port when planning voyages.",
      },
      position: "Fullstack Engineer",
      fullTime: "07/2021 - currently",
      tasks: [
        "Tech stack - Vue, Vuex, Vuetify, Nuxt, Node, SQL and Docker",
        "Updating existing API endpoints and creating new ones",
        "Creating unit tests for backend and ensuring all tests are passed",
        "Created server-side filtering for various admin facing tools",
      ],
    },

    {
      company: {
        name: "airBaltic",
        url: "https://www.airbaltic.com/en-LV/",
        location: "Riga, Latvia",
        description:
          "BunkerEx is an online portal for owners and operators to find their optimal bunker port when planning voyages.",
      },
      position: "Frontend Developer",
      fullTime: "11/2020 - 07/2021",
      tasks: [
        "Tech stack - Vue, Vuex, Pimcore and Docker",
        "Collaborated cohesively in a team of 7+ working with Agile methods",
        "Developed costumer self-service to improve costumers experience with adding new services and information to their flight",
        "Developed additional option for card payments",
        "Wrote unit test and performed regular code reviews",
        "Ensured universal browser (even IE11) and cross device combability",
      ],
    },

    {
      company: {
        name: "BunkerEx",
        url: "https://www.bunker-ex.com/",
        location: "Barcelona, Spain",
        description:
          "BunkerEx is an online portal for owners and operators to find their optimal bunker port when planning voyages.",
      },
      position: "Frontend Engineer",
      fullTime: "02/2020 - 11/2020",
      freelance: "11/2020 - 07/2021",
      tasks: [
        "Tech stack - Angular, Angular material, Ag-grid, Trading view",
        "Reporting and collaborating directly with CTO and CEO",
        "Developing web applications from the ground up for admin & customer use",
        "Created live trading screen (Trading view) and Grid (ag-grid) for fuel current and future prices",
        "Improving customer experience by seeking feedback and implementing multiple new features and adding material UI framework practices",
        "Tenacity and independence - Developing frontend autonomously",
        "Successfully built first application prototype within first week, without prior professional experience with necessary technologies",
      ],
    },

    {
      company: {
        name: "Helmes Latvia",
        url: "https://lv.helmes.com/",
        location: "Riga, Latvia",
        description:
          "One of the leading providers of integrated IT solutions and services in Latvia.",
      },
      position: "Fullstack Engineer",
      fullTime: "11/2018 – 01/2020",
      tasks: [
        "Tech stack - JavaScript, TypeScript, Node.js, Docker, Vue.js, SQL, ORM and Git",
        "Worked both autonomously and as part of a team of five people",
        "Independently created user friendly UI and API for CSV reports in one of the company’s main Finance projects",
        "Deployed projects using Gitlab pipelines, Docker and Kubernetes",
        "Wrote integration tests for database with Jest",
      ],
    },

    {
      company: {
        name: "SAF Tehnika",
        url: "https://www.saftehnika.com/",
        location: "Riga, Latvia",
        description:
          "A manufacturer of microwave data transmission equipment and measuring devices in Latvia.",
      },
      position: "Frontend Developer",
      fullTime: "02/2018 – 05/2018",
      tasks: [
        "Tech stack - Vue, Vuex and Vuetify",
        "Developed user dashboard to monitor sensor data",
        "Designed, planned and executed a UI mockups with a work-group",
      ],
    },
  ];

  public education = [
    {
      degree: "Bachelor of Engineering (B. Eng.)",
      major: "Computer Science",
      school: { name: "Latvia University of Life Sciences & Technologies" },
    },

    {
      degree: "Bachelor of Engineering (B. Eng.)",
      major: "Civil Engineering",
      school: { name: "Latvia University of Life Sciences & Technologies" },
    },
  ];

  constructor() {}
}
