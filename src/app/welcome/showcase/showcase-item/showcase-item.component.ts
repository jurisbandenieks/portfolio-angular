import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-showcase-item",
  templateUrl: "./showcase-item.component.html",
  styleUrls: ["./showcase-item.component.scss"]
})
export class ShowcaseItemComponent implements OnInit {
  @Input() info;

  constructor() {}

  ngOnInit(): void {}
}
