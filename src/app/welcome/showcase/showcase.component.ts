import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-showcase",
  templateUrl: "./showcase.component.html",
  styleUrls: ["./showcase.component.scss"],
})
export class ShowcaseComponent implements OnInit {
  public aboutInfo = [
    {
      frontText: "Tech Stack",
      frontIcon: "fas fa-laptop",
      rearText:
        "I have experience with wide array of Fullstack technologies and frameworks like - Angular, React, Vue, Gatsby, Node, Express, NoSQL, SQL and others",
    },
    {
      frontText: "Projects",
      frontIcon: "fas fa-book-open",
      rearText:
        "I enjoy building Web aplications and inovative projects which uses modern technologies and help to achieve the best result",
    },
    {
      frontText: "Skills",
      frontIcon: "fas fa-crosshairs",
      rearText:
        "Angular, Rxjs, React, Reducs, Context, Vue, Vuex, Gatsby, Node, SQL, NoSQL, API, GraphQL, AWS, Shopify, CMS, MongoDB, Docker, GIT",
    },
    {
      frontText: "Experience",
      frontIcon: "fas fa-running",
      rearText:
        "I have experience as Fullstack and Frontend Developer. Ranging from mostly design to full web application implementation with algorithms and databases. Whenever I do a personal project I usually use technologies that I am not using at full time work, opening myself to new ideas",
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
