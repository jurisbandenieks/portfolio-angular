import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "app-project-description",
  templateUrl: "./project-description.component.html",
  styleUrls: ["./project-description.component.scss"]
})
export class ProjectDescriptionComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ProjectDescriptionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { text: string }
  ) {}

  ngOnInit(): void {}
}
