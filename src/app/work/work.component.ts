import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-work",
  templateUrl: "./work.component.html",
  styleUrls: ["./work.component.scss"],
})
export class WorkComponent implements OnInit {
  projects = [
    {
      title: "Martina Ballite Shop",
      description: [
        "Currently WIP project which is subdomain of website www.martinaballite.lv and serves as e-shop.",
        "www.martinaballite.lv is built with Gatsby and works as statically built website, where admin is able to edit the content, by logging in it's Shopify account.",
        "Currently project works in test mode and is awaiting real life products for testing.",
        "Project gets built when a new product or product group gets added to colection",
        "Project code is private.",
      ],
      technologies: ["Gatsby", "React", "GraphQL", "Shopify", "JAM"],
      image: "assets/img/martinaballite-shop.png",
      gitlab: {
        code: "",
      },
      link: "https://peaceful-khorana-80a866.netlify.app/",
    },

    {
      title: "Petalia",
      description: [
        "Temperature monitoring system that enables user to see temperature data in browser.",
        "Project's architecture consist with ESP8266 boards reading the temperature with DS18b20 digital temperature sensors and sending that data to CloudMQTT Server/Broker. After data gets transmitted, a Node application, which is hosted on AWS, listens to data changes via CloudMQTT Websockets and according to sensor, writes data to Firestore database.",
        "And on the client side Vue application is reading this data, via it's Vuex store management. Data is visualised with Vuetify library.",
        "For 24 hour data visualization used D3.js library.",
      ],
      technologies: ["MQTT", "Firebase", "Node", "Vue", "Vuetify"],
      image: "assets/img/petalia.png",
      gitlab: {
        code: "https://gitlab.com/jurisbandenieks/petalia-frontend",
      },
      link: "https://petalia-cc61b.firebaseapp.com/",
    },

    {
      title: "Flagmania",
      description: [
        "Flag guessing game for people who are into Flags, like me.",
        "Application built with MERN stack. Frontend - React and Redux and on the Backend - Node, Express and MongoDB",
      ],
      technologies: ["React", "Redux", "Node", "SASS", "MongoDB"],
      image: "assets/img/flagmania.png",
      gitlab: {
        code: "https://gitlab.com/jurisbandenieks/flagmania-v2",
      },
      link: "https://flagmania-jurisbandenieks.netlify.app/",
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
